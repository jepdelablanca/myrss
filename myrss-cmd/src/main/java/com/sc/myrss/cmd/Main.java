package com.sc.myrss.cmd;

import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * Main class for the myrss commandline applcation.
 * 
 * This application should be an entry point to create passwords for users.
 *
 */
public class Main {

	public static void main(String[] args) {
		System.out.println("Hash for password is:");
		System.out.println(BCrypt.hashpw(args[0], BCrypt.gensalt()));
	}
}
