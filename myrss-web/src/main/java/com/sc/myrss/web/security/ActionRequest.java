package com.sc.myrss.web.security;

import java.io.Serializable;

public class ActionRequest implements Serializable {

	private static final long serialVersionUID = 7317536949669632052L;

	public ActionRequest() {}

	public ActionRequest(String action, String secResource) {
		this.action = action;
		this.secResource = secResource;
	}
	private String action;

	private String secResource;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getSecResource() {
		return secResource;
	}

	public void setSecResource(String secResource) {
		this.secResource = secResource;
	}

	@Override
	public String toString() {
		return "ActionRequest [action=" + action + ", secResource=" + secResource + "]";
	}

}
