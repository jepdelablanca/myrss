package com.sc.myrss.web.mvc;

import java.io.Serializable;

public class ErrorResponse implements Serializable {

	private static final long serialVersionUID = -6074892630767650168L;

	private String code;

	private String message;

	public ErrorResponse() {}

	public ErrorResponse(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
