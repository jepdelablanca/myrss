package com.sc.myrss.web.mvc.main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class WebUser implements Serializable {

	private static final long serialVersionUID = 8381704537384691339L;

	private String login;

	private String name;

	private String role;

	private String lang;

	private boolean nightMode;

	private List<String> securityResources = new ArrayList<String>();

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public boolean isNightMode() {
		return nightMode;
	}

	public void setNightMode(boolean nightMode) {
		this.nightMode = nightMode;
	}

	public List<String> getSecurityResources() {
		return securityResources;
	}

	public void setSecurityResources(List<String> securityResources) {
		this.securityResources = securityResources;
	}

}
