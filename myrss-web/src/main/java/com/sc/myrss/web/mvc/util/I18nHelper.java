package com.sc.myrss.web.mvc.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sc.myrss.web.util.ExposedResourceBundleMessageSource;

@Component
public class I18nHelper {

	private static final String MSG_BUNDLE_NAME = "com/sc/myrss/web/lang/i18n";

	@Autowired
	private ExposedResourceBundleMessageSource messageSource;

	public String getString(String key, Locale locale) {
		return messageSource.getbundle(MSG_BUNDLE_NAME, locale).getString(key);
	}

	public Map<String, String> bundleAsMap(Locale locale) {
		Map<String, String> result = new HashMap<String, String>();
		ResourceBundle bundle = messageSource.getbundle(MSG_BUNDLE_NAME, locale);
		Enumeration<String> keys = bundle.getKeys();
		while(keys.hasMoreElements()) {
			String key = keys.nextElement();
			result.put(key, bundle.getString(key));
		}
		return result;
	}

}
