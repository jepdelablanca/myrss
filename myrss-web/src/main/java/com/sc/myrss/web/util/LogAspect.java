package com.sc.myrss.web.util;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.aspectj.lang.Signature;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * Provee de trazas con argumentos y retorno (y excepción si ocurre)
 * de todas las invocaciones al activar el nivel DEBUG
 * de cualquier clase/método de myrss.<br/>
 * Code modified:<br/>
 * <ul>
 * <li>Moved to the appropiate package</li>
 * <li>Corrected text</li>
 * <li>Corrected time tracking</li>
 * <li>Simplified messages</li>
 * </ul>
 * @author Javier Moreno López javier.moreno.lopez@riseup.net
 */
//@Aspect
@Component
public class LogAspect {

	@Around("execution(* com.sc.myrss..*(..))")
	public Object logCall(ProceedingJoinPoint proceedingJointPoint) throws Throwable {
		Signature signature = proceedingJointPoint.getStaticPart().getSignature();
		
		String className = proceedingJointPoint.getTarget().getClass().getName();
		String methodName = signature.getName();
		Logger log = Logger.getLogger(className);
		if (!log.isDebugEnabled())
			return proceedingJointPoint.proceed(proceedingJointPoint.getArgs());
		Object[] args = proceedingJointPoint.getArgs();
		String argList = Arrays.toString(args);
		String invocation = String.format("%s(%s)", methodName, argList);
		log.debug(String.format("%s started.", invocation));
		long start = System.currentTimeMillis();
		try {
			Object result = proceedingJointPoint.proceed(args);
			long end = System.currentTimeMillis();
			log.debug(String.format("%s ended after %d ms returning: %s.", methodName, end - start, result));
			return result;
		} catch (RuntimeException t) {
			long end = System.currentTimeMillis();
			log.warn(String.format("%s threw runtime exception after %d ms. Error: %s.", methodName, end - start, t.getMessage()), t);
			throw t;
		} catch (Throwable t) {
			long end = System.currentTimeMillis();
			log.warn(String.format("%s threw exception after %d ms. Error: %s.", methodName, end - start, t.getMessage()));
			throw t;
		}
	}
}
