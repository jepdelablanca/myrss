package com.sc.myrss.web.mvc.exception;

public class ValidationException extends Exception {

	private static final long serialVersionUID = 1848364229203259716L;

	public ValidationException() {
		super();
	}

	public ValidationException(String message) {
		super(message);
	}

}
