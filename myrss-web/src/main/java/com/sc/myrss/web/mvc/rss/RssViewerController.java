package com.sc.myrss.web.mvc.rss;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sc.myrss.model.rss.Article;
import com.sc.myrss.model.rss.Subscription;
import com.sc.myrss.model.security.User;
import com.sc.myrss.neg.rss.reader.RssService;
import com.sc.myrss.web.WebConstants;
import com.sc.myrss.web.mvc.exception.ValidationException;
import com.sc.myrss.web.mvc.util.I18nHelper;

@RestController
@RequestMapping("/viewer")
public class RssViewerController {

	private static final String LBL_ALL_SUBSCRIPTIONS = "lbl_all_subscriptions";

	private static final String USERDATA_URL = "url";

	private static final String USERDATA_NAME = "name";

	private static final String USERDATA_DESCRIPTION = "description";

	private static final String USERDATA_ALL = "all";

	private static final String USERDATA_TYPE = "type";

	private static final String TYPE_CATEGORY = "category";

	private static final String TYPE_FEED = "feed";

	@Autowired
	private I18nHelper i18n;

	@Autowired
	private RssService rssService;

	@RequestMapping(value="/tree", method=RequestMethod.GET, produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody Tree getTree(Locale locale, HttpSession httpSession) {
		User user = (User) httpSession.getAttribute(WebConstants.USER_BEAN_SESSION_NAME);
		Locale userLocale = (Locale) httpSession.getAttribute(WebConstants.LOCALE_BEAN_SESSION_NAME);
		Tree result = new Tree();
		int itemId = 1;
		TreeItem allCategory = categoryFormatter(itemId++, 
				WebConstants.ALL_CATEGORY, i18n.getString(LBL_ALL_SUBSCRIPTIONS, userLocale));
		result.setItems(new ArrayList<TreeItem>());
		result.getItems().add(allCategory);
		List<TreeItem> uncategorized = new ArrayList<TreeItem>();
		Map<String, TreeItem> categories = new HashMap<String, TreeItem>();
		for(Subscription subscription : rssService.getSubscriptions(user.getLogin())) {
			allCategory.getItems().add(subscriptionFormatter(itemId++, subscription));
			String category = subscription.getCategory();
			if(category==null) {
				uncategorized.add(subscriptionFormatter(itemId++, subscription));
			} else {
				if(!categories.containsKey(category)) {
					categories.put(category, categoryFormatter(itemId++, category, category));
				}
				categories.get(category).getItems().add(subscriptionFormatter(itemId++, subscription));
			}
		}
		result.getItems().addAll(categories.values());
		result.getItems().addAll(uncategorized);
		return result;
	}

	@RequestMapping(value="/count", method=RequestMethod.POST, produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody Integer getItemCount(@RequestBody ItemForm form, Locale locale, HttpSession httpSession) throws ValidationException {
		User user = (User) httpSession.getAttribute(WebConstants.USER_BEAN_SESSION_NAME);
		String type = form.getType().trim();
		validateItemForm(form, httpSession);
		Integer result = null;
		if(TYPE_CATEGORY.equals(type)) {
			String category = WebConstants.ALL_CATEGORY.equals(form.getCategory())? null: form.getCategory();
			result = rssService.getCategoryUnreadCount(user.getLogin(), category);
		} else {
			result = rssService.getSubscriptionUnreadCount(user.getLogin(), form.getFeed().trim());
		}
		return result;
	}

	@RequestMapping(value="/subscription", method=RequestMethod.POST, produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody Subscription getSubscription(@RequestBody ItemForm form, Locale locale, HttpSession httpSession) throws ValidationException {
		User user = (User) httpSession.getAttribute(WebConstants.USER_BEAN_SESSION_NAME);
		validateItemForm(form, httpSession);
		return rssService.getSubscription(user.getLogin(), form.getFeed().trim());
	}

	@RequestMapping(value="/articleIds", method=RequestMethod.POST, produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody List<Long> getArticleIds(@RequestBody ItemForm form, HttpSession httpSession) throws ValidationException {
		User user = (User) httpSession.getAttribute(WebConstants.USER_BEAN_SESSION_NAME);
		validateItemForm(form, httpSession);
		String type = form.getType().trim();
		if(TYPE_CATEGORY.equals(type)) {
			String category = WebConstants.ALL_CATEGORY.equals(form.getCategory())? null: form.getCategory();
			return rssService.getCategoryArticleIds(user.getLogin(), category, form.getUnreadOnly());
		} else {
			return rssService.getSubscriptionArticleIds(user.getLogin(), form.getFeed(), form.getUnreadOnly());
		}
	}

	@RequestMapping(value="/articles", method=RequestMethod.POST, produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody List<Article> getArticles(@RequestBody List<Long> ids, HttpSession httpSession) {
		return rssService.getArticlesByIds(ids);
	}

	@RequestMapping(value="/article/{readId}/read/{readValue}", method=RequestMethod.GET, produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody boolean setArticleRead(@PathVariable Long readId, @PathVariable Boolean readValue) {
		rssService.markArticleAsRead(readId, readValue);
		return true;
	}

	@RequestMapping(value="/articles/read/{readValue}", method=RequestMethod.POST, produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody boolean setSubscriptionRead(@RequestBody ItemForm form, @PathVariable Boolean readValue, HttpSession httpSession) throws ValidationException {
		User user = (User) httpSession.getAttribute(WebConstants.USER_BEAN_SESSION_NAME);
		validateItemForm(form, httpSession);
		String type = form.getType().trim();
		if(TYPE_CATEGORY.equals(type)) {
			String category = WebConstants.ALL_CATEGORY.equals(form.getCategory())? null: form.getCategory();
			rssService.markCategoryAsRead(user.getLogin(), category, readValue);
		} else {
			rssService.markSubscriptionAsRead(user.getLogin(), form.getFeed(), readValue);
		}
		return true;
	}

	private TreeItem categoryFormatter(int id, String categoryId, String text) {
		TreeItem result = new TreeItem(""+ id, text);
		result.setItems(new ArrayList<TreeItem>());
		result.setUserdata(new HashMap<String, String>());
		result.getUserdata().put(USERDATA_TYPE, TYPE_CATEGORY);
		result.getUserdata().put(USERDATA_NAME, text);
		result.getUserdata().put(USERDATA_ALL, ""+ WebConstants.ALL_CATEGORY.equals(categoryId));
		return result;
	}

	private TreeItem subscriptionFormatter(int id, Subscription subscription) {
		TreeItem result = new TreeItem(""+ id, subscription.getTitle());
		result.setUserdata(new HashMap<String, String>());
		result.getUserdata().put(USERDATA_URL, subscription.getUrl());
		result.getUserdata().put(USERDATA_DESCRIPTION, subscription.getDescription());
		result.getUserdata().put(USERDATA_TYPE, TYPE_FEED);
		return result;
	}

	private void validateItemForm(ItemForm form, HttpSession httpSession) throws ValidationException {
		String type = form.getType().trim();
		if(type==null ||
				(!TYPE_CATEGORY.equals(type) && !TYPE_FEED.equals(type)) ||
				(TYPE_CATEGORY.equals(type) && (form.getCategory()==null || form.getCategory().trim().isEmpty())) ||
				(TYPE_FEED.equals(type) && (form.getFeed()==null || form.getFeed().trim().isEmpty()))) {
			Locale userLocale = (Locale) httpSession.getAttribute(WebConstants.LOCALE_BEAN_SESSION_NAME);
			throw new ValidationException(i18n.getString(WebConstants.MSG_ERR_INVALID_REQUEST, userLocale));
		}
	}
}
