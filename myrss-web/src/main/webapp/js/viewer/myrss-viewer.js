
/** RSS Feed viewer log. */
var rvLog = false;
var rvLogFiner = false;

/** CounterMap log. */
var cmLog = false;

function MYRSSViewer(app) {
	if(rvLog) console.log("[MYRSSViewer]");
	this.app = app;
	this.ui = {};
}

let viewerMethods = {
	
	init: function(callback) {
		if(rvLog) console.log("[MYRSSViewer][init]");
		this.initUI();
		this.loading = false;
		new AsyncQueue().
			enqueue(this.loadTree, this).
			setCallback($.proxy(this.selectTheALLCategory, this)).
			start();
	},
	
	destroy: function() {
		if(rvLog) console.log("[MYRSSViewer][destroy]");
	},
	
	resize: function() {
		if(rvLogFiner) console.log("[MYRSSViewer][resize]");
		if(this.ui.hasOwnProperty("layout") && this.ui.layout!=null) {
			this.ui.layout.setSizes();
		}
		this.resizeArticles();
	},
	
	initUI: function() {
		if(rvLog) console.log("[MYRSSViewer][initUI]");
		
		this.ui.layout = new dhtmlXLayoutObject({
			parent: this.app.ui.contents[0],
			pattern: "2U",
			offsets: {top: 10, right: 10, bottom: 10, left: 10},
			cells: [
				{id: "a", text: this.app.translateText("lbl_navigation_tree"), collapsed_text: this.app.translateText("lbl_navigation_tree"), header: true, width: 400},
				{id: "b", text: this.app.translateText("lbl_article_list"), header: true}
			]
		});
		this.ui.layout.attachEvent("onCollapse", $.proxy(function() {
			this.resizeArticles();
		}, this));
		this.ui.layout.attachEvent("onExpand", $.proxy(function() {
			this.resizeArticles();
		}, this));
		this.ui.layout.attachEvent("onResizeFinish", $.proxy(function() {
			this.resizeArticles();
		}, this));
		this.ui.layout.attachEvent("onPanelResizeFinish", $.proxy(function() {
			this.resizeArticles();
		}, this));
		
		this.ui.treeCell = this.ui.layout.cells("a");
		this.ui.treeCell.setCollapsedText("");
		this.ui.browseCell = this.ui.layout.cells("b");
		$(this.ui.treeCell.cell).addClass("treeCell");
		$(this.ui.browseCell.cell).addClass("browseCell");
		this.ui.browseCell.hideArrow();
		
		this.ui.articleListToolbar = this.ui.browseCell.attachToolbar({iconset: "awesome"});
		this.ui.articleListToolbar.addText("titleHolder", 0, "<span class='titleHolder'></span>");
		this.ui.titleHolder = $(this.ui.articleListToolbar.base).find(".titleHolder");
		this.ui.articleListToolbar.addSeparator("sep1", 1);
		this.ui.articleListToolbar.addButton("btnReload", 2, null, "fa fa-refresh", "fa fa-refresh");
		this.ui.articleListToolbar.setItemToolTip("btnReload", this.app.translateText("btn_reload"));
		this.ui.articleListToolbar.disableItem("btnReload");
		this.ui.articleListToolbar.addSeparator("sep2", 3);
		this.ui.articleListToolbar.addButton("btPrevArticle", 4, null, "fa fa-chevron-circle-up", "fa fa-chevron-circle-up");
		this.ui.articleListToolbar.addButton("btNextArticle", 5, null, "fa fa-chevron-circle-down", "fa fa-chevron-circle-down");
		this.ui.articleListToolbar.setItemToolTip("btPrevArticle", this.app.translateText("btn_previous_article"));
		this.ui.articleListToolbar.setItemToolTip("btNextArticle", this.app.translateText("btn_next_article"));
		this.ui.articleListToolbar.disableItem("btPrevArticle");
		this.ui.articleListToolbar.disableItem("btNextArticle");
		this.ui.articleListToolbar.addSeparator("sep2", 6);
		this.ui.articleListToolbar.addButton("btMarkAllRead", 7, this.app.translateText("btn_mark_all_read")+"&nbsp;", "fa fa-thumbs-o-up", "fa fa-thumbs-o-up");
		this.ui.articleListToolbar.disableItem("btMarkAllRead");
		this.ui.articleListToolbar.attachEvent("onClick", $.proxy(function(name) {
			if("btnReload"===name) {
				let itemId = this.ui.tree.getSelectedId();
				this.ui.tree.unselectItem(itemId);
				this.ui.tree.selectItem(itemId, false, false);
			} else if("btPrevArticle"===name) {
				this.selectThePreviousArticle();
			} else if("btNextArticle"===name) {
				this.selectTheNextArticle();
			} else if("btMarkAllRead"===name) {
				this.confirmMarkAllAsRead();
			}
		}, this));
		
		this.ui.scrollContainer = $(this.ui.browseCell.cell).find(">.dhx_cell_cont_layout");
		this.ui.scrollablePanel = $("<div class=\"scrollablePanel\"></div>");
		this.ui.articlesContainer = $("<div class=\"articlesContainer\"></div>");
		this.ui.scrollContainer.append(this.ui.scrollablePanel);
		this.ui.scrollablePanel.append(this.ui.articlesContainer);
		this.ui.articleList = new dhtmlXAccordion({
			parent: this.ui.articlesContainer[0],
			multi_mode: true,
			items: []
		});
		this.ui.articleList.attachEvent("onBeforeActive", $.proxy(function(itemId, state) {
			this.selectAccordionItem(itemId);
			this.refreshNavigationButtons();
			return false;
		}, this));
		this.ui.articlesContainer.height(0);
		this.ui.browseCell.showHeader();
		
		this.ui.bottomReached = function(view) {
			view.loadNextArticles($.proxy(function() {
				this.refreshNavigationButtons();
			}, view));
		};
		this.ui.scrollablePanel.on("scroll", $.proxy(function() {
			let element = this.ui.scrollablePanel[0];
			let atBottom = element.scrollHeight - element.scrollTop === element.clientHeight;
			if(atBottom) {
				window.setTimeout(this.ui.bottomReached, 100, this);
			}
		}, this));
		
		this.ui.treeToolbar = this.ui.treeCell.attachToolbar({iconset: "awesome"});
		this.ui.treeToolbar.addButton("btnReload", 1, null, "fa fa-refresh", "fa fa-refresh");
		this.ui.treeToolbar.setItemToolTip("btnReload", this.app.translateText("btn_reload"));
		this.ui.treeToolbar.addSeparator("sep1", 2);
		this.ui.treeToolbar.addButton("btnCollapseAll", 3, null, "fa fa-minus-circle", "fa fa-minus-circle");
		this.ui.treeToolbar.setItemToolTip("btnCollapseAll", this.app.translateText("btn_collapseAll"));
		this.ui.treeToolbar.addButton("btnExpandAll", 4, null, "fa fa-plus-circle", "fa fa-plus-circle");
		this.ui.treeToolbar.setItemToolTip("btnExpandAll", this.app.translateText("btn_expandAll"));
		this.ui.treeToolbar.addSeparator("sep2", 5);
		this.ui.treeToolbar.addButtonTwoState("showRead", 6, this.app.translateText("btn_show_read")+"&nbsp;", "fa fa-hand-o-right", "fa fa-hand-o-right");
		this.ui.treeToolbar.attachEvent("onClick", $.proxy(function(name) {
			if("btnExpandAll"===name) {
				for(let itemId of Object.keys(this.ui.tree.items)) {
					let item = this.ui.tree.items[itemId];
					if(item.kids) {
						this.ui.tree.openItem(itemId);
					}
				}
			} else if("btnCollapseAll"===name) {
				for(let itemId of Object.keys(this.ui.tree.items)) {
					let item = this.ui.tree.items[itemId];
					if(item.kids) {
						this.ui.tree.closeItem(itemId);
					}
				}
			} else if("btnReload"===name) {
				this.loadTree($.proxy(function(itemId) {
					this.ui.tree.selectItem(itemId, true, false);
				}, this, this.ui.tree.getSelectedId()));
			}
		}, this));
		this.ui.treeToolbar.attachEvent("onStateChange", $.proxy(function(name) {
			if("showRead"===name) {
				this.ui.tree.showItem(this.ui.tree.getSelectedId());
				this.loadTree($.proxy(function(itemId) {
					this.ui.tree.selectItem(itemId, true, false);
				}, this, this.ui.tree.getSelectedId()));
			}
		}, this));
		
		this.ui.tree = this.ui.treeCell.attachTreeView({});
		this.ui.tree.cancelClick = false;
		this.ui.tree.attachEvent("onClick", $.proxy(function(itemId) {
			this.ui.tree.cancelClick = false;
			setTimeout($.proxy(function() {
				if(this.ui.tree.cancelClick) {
					return false;
				} else {
					let itemIsSelected = (itemId==this.ui.tree.getSelectedId());
					if(itemIsSelected) {
						this.ui.tree.unselectItem(itemId);
						this.ui.tree.selectItem(itemId, false, false);
					}
					return true;
				}
			}, this), 200);
		}, this));
		this.ui.tree.attachEvent("onDblClick", $.proxy(function(itemId) {
			this.ui.tree.cancelClick = true;
			if("category"===this.ui.tree.getUserData(itemId, "type")) {
				this.toggleTreeCategory(itemId);
			}
		}, this));
		this.ui.tree.attachEvent("onSelect", $.proxy(function(itemId, selected) {
			if(selected===true) {
				let tree = this.ui.tree;
				let type = tree.getUserData(itemId, "type");
				if("category"===type) {
					let catId = null;
					if("true"===tree.getUserData(itemId, "all")) {
						catId = "ALL";
					} else {
						catId = tree.getUserData(itemId, "name");
					}
					this.loadCategory(catId);
				} else if("feed"===type) {
					let url = tree.getUserData(itemId, "url");
					this.loadSubscription(url);
				}
			}
		}, this));
	},
	
	loadTree: function(callback) {
		if(rvLog) console.log("[MYRSSViewer][loadTree]");
		let payload = {
			view: this,
			callback: callback
		};
		this.ui.tree.clearAll();
		this.counters = {};
		this.categories = {};
		this.treeItems = {};
		this.allCategoryItemId = null;
		$.ajax({
			method: "GET",
			url: "mvc/viewer/tree",
			dataType: "json",
			success: $.proxy(function(data) {
				let tree = this.view.ui.tree;
				tree.loadStruct(data.items);
				
				this.view.traverseTree(tree, $.proxy(function(itemId) {
					if(itemId!=null) {
						let tree = this.ui.tree;
						let type = tree.getUserData(itemId, "type");
						let identifier = {
							type: type
						};
						let count = null;
						if("category"===type) {
							let text = tree.getItemText(itemId);
							let code = 
								"<span class=\"treeItemText\" data-ri=\""+ itemId +"\" style=\"pointer-events: none;\">"
									+ "<span class=\"count\"></span>"
									+ "&nbsp;"+ text
								+"</span>";
							tree.setItemText(itemId, code);
							let name = tree.getUserData(itemId, "name");
							identifier.category = name;
							if("true"===tree.getUserData(itemId, "all")) {
								this.allCategoryItemId = itemId;
								identifier.category = "ALL";
							}
							count = $(tree.base).find("[data-ri=\""+ itemId +"\"]>.count");
							let item = count.parent().parent().parent().parent();
							this.treeItems[itemId] = item;
							this.categories[itemId] = item;
							item.identifier = identifier;
						} else if("feed"===type) {
							let text = tree.getItemText(itemId);
							let description = tree.getUserData(itemId, "description");
							let code =
								"<span class=\"treeItemText\" title=\""+ description +"\" data-ri=\""+ itemId +"\" style=\"pointer-events: none;\">"
									+ "<span class=\"count\"></span>"
									+ "&nbsp;<i>"+ text +"</i>"
								+ "</span>";
							tree.setItemText(itemId, code);
							identifier.feed = tree.getUserData(itemId, "url");
							count = $(tree.base).find("[data-ri=\""+ itemId +"\"]>.count");
							let item = count.parent().parent().parent().parent();
							this.treeItems[itemId] = item;
							item.identifier = identifier;
						}
						this.makeCount(count, identifier);
						count.onCountUpdate = $.proxy(function(itemId) {
							let isAllCategory = this.payload.type=="category" && this.payload.category=="ALL";
							let showReadFlag = this.view.ui.treeToolbar.getItemState("showRead");
							let hasItems = Number(this.countValue)>0;
							if(hasItems || isAllCategory || showReadFlag) {
								this.view.treeItems[itemId].show();
							} else {
								this.view.treeItems[itemId].hide();
								if(itemId==this.view.ui.tree.getSelectedId()) {
									this.view.clearArticles();
									this.view.selectTreeItemNextTo(itemId);
								}
							}
						}, count, itemId);
						this.counters[itemId] = count;
					}
				}, this.view));
				
				this.view.treeCounterMap = new CounterMap();
				this.view.traverseTree(tree, $.proxy(function(itemId) {
					if(itemId!=null) {
						let counter = this.counters[itemId];
						let item = this.treeItems[itemId];
						let identifier = item.identifier;
						this.treeCounterMap.addCounter(identifier, counter);
						let parentId = this.ui.tree.getParentId(itemId);
						if(parentId!=undefined) {
							let parentCounter = this.counters[parentId];
							this.treeCounterMap.addCounter(identifier, parentCounter);
						}
					}
				}, this.view));
				
				for(let itemId of Object.keys(this.view.counters)) {
					this.view.counters[itemId].recount();
				}
				
				this.callback();
			}, payload),
			error: $.proxy(function(xhr) {
				if(this.view.app.processErrorResponse(xhr)) {
					this.callback();
				}
			}, payload)
		});
	},
	
	selectTreeItemNextTo(itemId) {
		if(rvLog) console.log("[MYRSSViewer][selectTreeItemNextTo](itemId: "+ itemId +")");
		let nextItemId = null;
		
		let element = $(this.ui.tree.items[itemId].item);
		let next = this.findNextVisible(element);
		if(next==null) {
			let parentId = this.ui.tree.getParentId(itemId);
			if(parentId!=undefined) {
				let parentElement = $(this.ui.tree.items[parentId].item);
				next = this.findNextVisible(parentElement);
			}
		}
		if(next!=null) {
			nextItemId = next[0]._itemId;
		}
		
		this.ui.tree.unselectItem(this.ui.tree.getSelectedId());
		if(nextItemId!=null) {
			this.ui.tree.selectItem(nextItemId, false, false);
		}
	},
	
	findNextVisible: function(jItem) {
		if(rvLog) console.log("[findNextVisible]");
		let result = null;
		if(jItem!=null && jItem.length>0) {
			let next = jItem.next();
			while(next.length>0 && next.is(":hidden")) {
				next = next.next();
			}
			if(next.length>0 && next.is(":visible")) {
				result = next;
			}
		}
		return result;
	},
	
	toggleTreeCategory: function(itemId) {
		if(rvLog) console.log("[MYRSSViewer][toggleTreeCategory](itemId: "+ itemId +")");
		if(itemId && itemId!=null) {
			if(this.categories.hasOwnProperty(itemId)) {
				let item = this.categories[itemId];
				let kids = item.find(">.dhxtreeview_kids_cont");
				let isClosed = kids.css("opacity")==0;
				if(rvLog) console.log("[MYRSSViewer][toggleTreeCategory] isClosed: "+ isClosed);
				if(isClosed) {
					this.ui.tree.openItem(""+itemId);
				} else {
					this.ui.tree.closeItem(""+itemId);
				}
			}
		}
	},
	
	traverseTree: function(tree, operation) {
		let id = (arguments.length>2)? arguments[2]: null;
		if(rvLogFiner) console.log("[MYRSSViewer][traverseTree]"+ (id!=null? "(id: "+ id +")": ""));
		let children = tree.getSubItems(id);
		for(let chId of children) {
			this.traverseTree(tree, operation, chId);
		}
		operation(id);
	},
	
	selectTheALLCategory: function() {
		if(rvLog) console.log("[MYRSSViewer][selectTheALLCategory]");
		if(this.allCategoryItemId!=null) {
			this.ui.tree.selectItem(this.allCategoryItemId);
		}
	},
	
	loadSubscription: function(url, callback) {
		if(rvLog) console.log("[MYRSSViewer][loadSubscription](url: "+ url +")");
		this.clearArticles();
		this.viewing.currentForm = {
			type: "feed",
			feed: url
		};
		new AsyncQueue().
			enqueue(this.showSubscription, this).
			enqueue(this.loadArticleIds, this).
			enqueue(this.loadNextArticles, this).
			enqueue(this.selectTheFirstArticle, this).
			enqueue(this.refreshNavigationButtons, this).
			setCallback(callback || null).
			start();
	},
	
	showSubscription: function(callback) {
		if(rvLog) console.log("[MYRSSViewer][showSubscription]");
		let payload = {
			view: this,
			callback: callback || null
		};
		this.downloadSubscription(
			this.viewing.currentForm.feed,
			$.proxy(function(data) {
				this.view.paintSubscription(data);
				if(this.callback!=null) {
					this.callback();
				}
			}, payload)
		);
	},
	
	downloadSubscription: function(subscription, callback) {
		if(rvLog) console.log("[MYRSSViewer][downloadSubscription]");
		let payload = {
			view: this,
			callback: callback || null
		};
		let form = {
			type: "feed",
			feed: subscription
		};
		$.ajax({
			method: "POST",
			url: "mvc/viewer/subscription",
			data: JSON.stringify(form),
			dataType: "json",
			contentType: "application/json",
			success: $.proxy(function(data) {
				if(this.callback!=null) {
					this.callback(data);
				}
			}, payload),
			error: $.proxy(function(xhr) {
				if(this.view.app.processErrorResponse(xhr)) {
					if(this.callback!=null) {
						this.callback(null); // This is right!
					}
				}
			}, payload)
		});
	},
	
	loadCategory: function(category, callback) {
		if(rvLog) console.log("[MYRSSViewer][loadCategory](category: "+ category +")");
		this.clearArticles();
		this.viewing.currentForm = {
			type: "category",
			category: category
		};
		this.paintCategory(category);
		new AsyncQueue().
			enqueue(this.loadArticleIds, this).
			enqueue(this.loadNextArticles, this).
			enqueue(this.selectTheFirstArticle, this).
			enqueue(this.refreshNavigationButtons, this).
			setCallback(callback || null).
			start();
	},
	
	paintSubscription: function(subscription) {
		if(rvLog) console.log("[MYRSSViewer][paintSubscription]");
		this.ui.titleHolder.html("<span class=\"siContainer\"></span>&nbsp;<span class=\"count\"></span>");
		
		let cell = $(this.ui.browseCell.cell);
		let siContainer = this.ui.titleHolder.find(".siContainer");
		let composerFunction = $.proxy(function(loadResult) {
			if(loadResult.success) {
				subscription = loadResult.value;
				this.view.renderSubscriptionInfo(subscription, this.target);
			}
		}, {view: this, target: siContainer});
		this.viewing.subscriptionCache.runWithCache(subscription.url, composerFunction);
		
		this.viewing.currentCount = this.ui.titleHolder.find(".count");
		this.makeCount(this.viewing.currentCount, this.viewing.currentForm);
		this.viewing.currentCount.recount();
	},
	
	paintCategory: function(category) {
		if(rvLog) console.log("[MYRSSViewer][paintCategory](category: "+ category +")");
		let title = ("ALL"===category)? this.app.translateText("lbl_all_subscriptions"): category;
		this.ui.titleHolder.html(
			"<span class=\"listTitle\">" +
				"<i class='fa fa-folder-open-o'></i>" +
				"&nbsp;&nbsp;<b>["+ title +"]</b>" +
				"&nbsp;<span class=\"count\"></span>" +
			"</span>"
		);
		this.viewing.currentCount = this.ui.titleHolder.find(".count");
		this.makeCount(this.viewing.currentCount, this.viewing.currentForm);
		this.viewing.currentCount.recount();
	},
	
	cacheSubscription: function(subscription, callback) {
		if(rvLog) console.log("[MYRSSViewer][cacheSubscription]");
		DataCache.postHelper(subscription, "mvc/viewer/subscription", {type: "feed", feed: subscription}, callback);
	},
	
	clearArticles: function() {
		if(rvLog) console.log("[MYRSSViewer][clearArticles]");
		this.ui.articleList.forEachItem($.proxy(function(cell) {
			let cellId = cell._idd;
			this.ui.articleList.removeItem(cellId);
		}, this));
		this.ui.titleHolder.html("");
		this.ui.articlesContainer.height(0);
		this.ui.articleListToolbar.disableItem("btnReload");
		this.ui.articleListToolbar.disableItem("btPrevArticle");
		this.ui.articleListToolbar.disableItem("btNextArticle");
		this.ui.articleListToolbar.disableItem("btMarkAllRead");
		this.viewing = {
			currentForm: null,
			currentCount: null,
			currentArticleItemId: null,
			articleIds: [],
			lastArticleId: 1,
			subscriptionCache: new DataCache("subscriptionCache", $.proxy(this.cacheSubscription, this)),
		};
	},
	
	refreshNavigationButtons: function(callback) {
		if(rvLog) console.log("[MYRSSViewer][refreshNavigationButtons]");
		if(!this.isTheFirstArticle()) {
			this.ui.articleListToolbar.enableItem("btPrevArticle");
		} else {
			this.ui.articleListToolbar.disableItem("btPrevArticle");
		}
		if(!this.isTheLastArticle()) {
			this.ui.articleListToolbar.enableItem("btNextArticle");
		} else {
			this.ui.articleListToolbar.disableItem("btNextArticle");
		}
		if(this.viewing.currentArticleItemId!=null) {
			this.ui.articleListToolbar.enableItem("btnReload");
			this.ui.articleListToolbar.enableItem("btMarkAllRead");
		}
		if(callback!=null) {
			callback();
		}
	},
	
	isTheFirstArticle: function() {
		return (
			(this.viewing.currentArticleItemId==null) ||
			(this.viewing.currentArticleItemId==1)
		);
	},
	
	isTheLastArticle: function() {
		return (
			(this.viewing.currentArticleItemId==null) ||
			(
				(this.viewing.currentArticleItemId==this.viewing.lastArticleId-1) &&
				(this.viewing.articleIds.length==0)
			)
		);
	},
	
	selectTheFirstArticle: function(callback) {
		if(rvLog) console.log("[MYRSSViewer][selectTheFirstArticle]");
		this.selectAccordionItem(1);
		if(callback!=null) {
			callback();
		}
	},
	
	selectTheNextArticle: function() {
		if(rvLog) console.log("[MYRSSViewer][selectTheNextArticle]");
		if(this.viewing.currentArticleItemId!=null) {
			let nextId = this.viewing.currentArticleItemId+1;
			if(this.selectAccordionItem(nextId)) {
				this.ui.articleList.cells(nextId).cell.scrollIntoView(
						{behavior: "smooth", block: "start"});
				this.refreshNavigationButtons();
			} else { // more articles need to be loaded
				this.loadNextArticles($.proxy(function(nextId) {
					this.refreshNavigationButtons();
					if(this.selectAccordionItem(nextId)) {
						this.ui.articleList.cells(nextId).cell.scrollIntoView(
								{behavior: "smooth", block: "start"});
						this.refreshNavigationButtons();
					}
				}, this, nextId));
			}
		}
	},
	
	selectThePreviousArticle: function() {
		if(rvLog) console.log("[MYRSSViewer][selectThePreviousArticle]");
		if(this.viewing.currentArticleItemId!=null && !this.isTheFirstArticle()) {
			let prevId = this.viewing.currentArticleItemId-1;
			if(this.selectAccordionItem(prevId)) {
				this.ui.articleList.cells(prevId).cell.scrollIntoView(
					{behavior: "smooth", block: "start"});
				this.refreshNavigationButtons();
			}
		}
	},
	
	selectAccordionItem: function(itemId) {
		if(rvLog) console.log("[MYRSSViewer][selectAccordionItem](itemId: "+ itemId +")");
		$(this.ui.articleList.cont).find(".dhx_cell_acc.selected").each(function(index, element) {
			$(element).removeClass("selected");
		});
		try {
			let item = this.ui.articleList.cells(itemId);
			$(item.cell).addClass("selected");
			this.viewing.currentArticleItemId = itemId;
			return true;
		} catch(e) {
			return false;
		}
	},
	
	loadArticleIds: function(callback) {
		if(rvLog) console.log("[MYRSSViewer][loadArticleIds]");
		let payload = {
			view: this,
			callback: callback || null
		};
		this.viewing.articleIds = [];
		let showReadFlag = this.ui.treeToolbar.getItemState("showRead");
		this.viewing.currentForm.unreadOnly = !showReadFlag;
		$.ajax({
			method: "POST",
			url: "mvc/viewer/articleIds",
			data: JSON.stringify(this.viewing.currentForm),
			dataType: "json",
			contentType: "application/json",
			success: $.proxy(function(data) {
				this.view.viewing.articleIds = data;
				if(this.callback!=null) {
					this.callback();
				}
			}, payload),
			error: $.proxy(function(xhr) {
				if(this.view.app.processErrorResponse(xhr)) {
					if(this.callback!=null) {
						this.callback();
					}
				}
			}, payload)
		});
	},
	
	loadNextArticles: function(callback) {
		if(rvLog) console.log("[MYRSSViewer][loadNextArticles]");
		let AMOUNT = 10;
		if(!this.loading && this.viewing.articleIds!=null && this.viewing.articleIds.length>0) {
			this.loading = true;
			let idsToLoad = this.viewing.articleIds.slice(0, AMOUNT);
			let payload = {view: this, callback: callback || null};
			$.ajax({
				method: "POST",
				url: "mvc/viewer/articles",
				data: JSON.stringify(idsToLoad),
				dataType: "json",
				contentType: "application/json",
				success: $.proxy(function(data) {
					for(let article of data) {
						let pos = this.view.viewing.articleIds.indexOf(article.readId);
						this.view.viewing.articleIds.splice(pos, 1);
					}
					this.view.loading = false;
					this.view.showArticles(data, this.callback);
				}, payload),
				error: $.proxy(function(xhr) {
					this.view.loading = false;
					if(this.view.app.processErrorResponse(xhr)) {
						if(this.callback!=null) {
							this.callback();
						}
					}
				}, payload)
			});
		} else {
			if(callback!=null) {
				callback();
			}
		}
	},
	
	showArticles: function(articleList, callback) {
		if(rvLog) console.log("[MYRSSViewer][showArticles](count: "+ (articleList.length) +")");
		for(let article of articleList) {
			let title = (article.title!=null && article.title.length>0)? 
				this.escapeHtml(article.title): this.app.translateText("lbl_no_title");
			this.ui.articleList.addItem(this.viewing.lastArticleId, title, true, 100, null);
			let articlePanel = this.ui.articleList.cells(this.viewing.lastArticleId);
			articlePanel.progressOn();
			
			articlePanel.cellId = this.viewing.lastArticleId;
			articlePanel.articleId = article.readId;
			let cell = $(articlePanel.cell);
			if(article.read) {
				cell.addClass("read");
			}
			
			cell.find(".dhx_cell_hdr_arrow").hide();
			cell.find(".dhx_cell_hdr_text").attr("title", title);
			articlePanel.toolbar = articlePanel.attachToolbar({iconset: "awesome"});
			let toolbarCount = 0;
			
			if("category"==this.viewing.currentForm.type) {
				articlePanel.toolbar.addText("subscriptionInfo", toolbarCount++, "<span class='siContainer'></span>");
				articlePanel.subscriptionInfoContainer = cell.find("span.siContainer");
				let composerFunction = $.proxy(function(loadResult) {
					if(loadResult.success) {
						subscription = loadResult.value;
						this.view.renderSubscriptionInfo(subscription, this.target);
					}
				}, {view: this, target: articlePanel.subscriptionInfoContainer});
				this.viewing.subscriptionCache.runWithCache(article.subscriptionUrl, composerFunction);
				articlePanel.toolbar.addSeparator("sep"+toolbarCount, toolbarCount++);
			}
			
			articlePanel.toolbar.addButton("viewOnline", toolbarCount++, this.app.translateText("btn_view_article")+"&nbsp;", "fa fa-external-link", "fa fa-external-link");
			articlePanel.toolbar.addButton("markRead", toolbarCount++, this.app.translateText("btn_mark_read")+"&nbsp;", "fa fa-thumbs-o-up", "fa fa-thumbs-o-up");
			articlePanel.toolbar.addButton("markUnread", toolbarCount++, this.app.translateText("btn_mark_unread")+"&nbsp;", "fa fa-thumbs-o-down", "fa fa-thumbs-o-down");
			if(article.read) {
				articlePanel.toolbar.hideItem("markRead");
			} else {
				articlePanel.toolbar.hideItem("markUnread");
			}
			articlePanel.toolbar.attachEvent("onClick", $.proxy(function(name) {
				this.view.selectAccordionItem(this.panel.cellId);
				if("viewOnline"===name) {
					if(this.article.sourceUrl!=null) {
						window.open(this.article.sourceUrl, "_blank");
					}
				} else if("markRead"===name) {
					this.view.setArticleReadFlag(this.article.readId, true, $.proxy(function() {
						$(this.panel.cell).addClass("read");
						this.panel.toolbar.hideItem("markRead");
						this.panel.toolbar.showItem("markUnread");
						this.view.selectTheNextArticle();
						// Counter at top of article list
						this.view.viewing.currentCount.recount();
						// Counters at tree
						let itemId = this.view.ui.tree.getSelectedId();
						let identifier = {
							type: "feed",
							feed: article.subscriptionUrl
						};
						let counters = this.view.treeCounterMap.getCounters(identifier);
						for(let counter of counters) {
							counter.recount();
						}
					}, this));
				} else if("markUnread"===name) {
					this.view.setArticleReadFlag(this.article.readId, false, $.proxy(function() {
						$(this.panel.cell).removeClass("read");
						this.panel.toolbar.hideItem("markUnread");
						this.panel.toolbar.showItem("markRead");
						// Counter at top of article list
						this.view.viewing.currentCount.recount();
						// Counters at tree
						let itemId = this.view.ui.tree.getSelectedId();
						let identifier = {
							type: "feed",
							feed: article.subscriptionUrl
						};
						let counters = this.view.treeCounterMap.getCounters(identifier);
						for(let counter of counters) {
							counter.recount();
						}
					}, this));
				}
			}, {view: this, panel: articlePanel, article: article}));
			
			let hasContent =
				article.hasOwnProperty("content") &&
				article.content!=undefined && 
				article.content!=null && 
				article.content.length>0;
			if(hasContent) {
				let jCell = $(articlePanel.cell);
				let content = jCell.find(">.dhx_cell_cont_acc");
				this.callbackWhenLoaded(content[0], $.proxy(function(panel) {
					if(panel._idd!=null) {
						// Adjust panel images size
						this.adjustPanelImages(panel);
						this.adjustPanelHeight(panel);
						this.adjustPanelContainerHeight();
						panel.progressOff();
					}
				}, this, articlePanel));
				articlePanel.attachHTMLString(article.content);
			} else {
				articlePanel.progressOff();
			}
			this.adjustPanelHeight(articlePanel);
			this.adjustPanelContainerHeight();
			this.viewing.lastArticleId++;
		}
		
		if(callback && callback!=null) {
			callback();
		}
	},
	
	adjustPanelImages: function(articlePanel) {
		if(rvLogFiner) console.log("[MYRSSViewer][adjustPanelImages]");
		let content = $(articlePanel.cell).find(">.dhx_cell_cont_acc");
		let images = content.find("img");
		for(let c=0; c<images.length; c++) {
			let image = $(images[c]);
			if(!image.is("[originalWidth]")) {
				image.attr("originalWidth", image.width());
				image.attr("originalHeight", image.height());
			}
			let imageWidth = Number(image.attr("originalWidth"));
			let imageHeight = Number(image.attr("originalHeight"));
			let cnip = image.parent();
			let isInline = (cnip.css("display")=="inline");
			let isPanel = (cnip[0].isSameNode(articlePanel.cell));
			while(isInline && !isPanel) {
				cnip = cnip.parent();
				isInline = (cnip.css("display")=="inline");
				isPanel = (cnip[0].isSameNode(articlePanel.cell));
			}
			let paddingLeft = parseInt(cnip.css("padding-left"), 10);
			let paddingRight = parseInt(cnip.css("padding-right"), 10);
			let parentWidth = cnip.width() - paddingLeft - paddingRight;
			let scale = 1;
			if(imageWidth>parentWidth) {
				scale = (parentWidth/imageWidth);
			}
			image.width(imageWidth*scale);
			image.height(imageHeight*scale);
		}
	},
	
	resizeArticles: function() {
		if(rvLogFiner) console.log("[MYRSSViewer][resizeArticles]");
		if(this.ui.hasOwnProperty("articleList") && this.ui.articleList!=null) {
			this.ui.articleList.setSizes();
			this.ui.articleList.forEachItem($.proxy(function(cell) {
				this.adjustPanelImages(cell);
				this.adjustPanelHeight(cell);
			}, this));
			this.adjustPanelContainerHeight();
		}
	},
	
	adjustPanelHeight: function(articlePanel) {
		if(rvLogFiner) console.log("[MYRSSViewer][adjustPanelHeight]");
		let jCell = $(articlePanel.cell);
		let header = jCell.find(">.dhx_cell_hdr")[0];
		let content = jCell.find(">.dhx_cell_cont_acc");
		content.height(0); // fix
		let height = 
			header.scrollHeight +
			content[0].scrollHeight +
			48;
		articlePanel.setHeight(height);
	},
	
	adjustPanelContainerHeight: function() {
		if(rvLogFiner) console.log("[MYRSSViewer][adjustPanelContainerHeight]");
		let container = $(this.ui.articleList.cont);
		let base = $(this.ui.articleList.base);
		container.height(0);
		let desired = container[0].scrollHeight;
		container.height(desired);
		base.height(desired);
	},
	
	setArticleReadFlag: function(articleId, readValue, callback) {
		if(rvLog) console.log("[MYRSSViewer][setArticleReadFlag](articleId: "+ articleId +", readValue: "+ readValue +")");
		let payload = {view: this, callback: callback || null};
		$.ajax({
			method: "GET",
			url: "mvc/viewer/article/"+ articleId +"/read/"+ readValue,
			success: $.proxy(function(data) {
				if(this.callback!=null) {
					this.callback();
				}
			}, payload),
			error: $.proxy(function(xhr) {
				if(this.view.app.processErrorResponse(xhr)) {
					if(this.callback!=null) {
						this.callback();
					}
				}
			}, payload)
		});
	},
	
	markAllAsRead: function() {
		if(rvLog) console.log("[MYRSSViewer][markAllAsRead]");
		$.ajax({
			method: "POST",
			url: "mvc/viewer/articles/read/true",
			data: JSON.stringify(this.viewing.currentForm),
			dataType: "json",
			contentType: "application/json",
			success: $.proxy(function(data) {
				// Counter at top of article list
				this.viewing.currentCount.recount();
				// Counters at tree
				let itemId = this.ui.tree.getSelectedId();
				let item = this.treeItems[itemId];
				let identifier = item.identifier;
				let counters = this.treeCounterMap.getCounters(identifier);
				for(let counter of counters) {
					counter.recount();
				}
			}, this),
			error: $.proxy(function(xhr) {
				this.app.processErrorResponse(xhr);
			}, this)
		});
	},
	
	confirmMarkAllAsRead: function() {
		if(rvLog) console.log("[MYRSSViewer][confirmMarkAllAsRead]");
		let text =
			this.viewing.currentForm.type==="category"?
			"msg_confirm_mark_all_read_category": "msg_confirm_mark_all_read_feed";
		let name = null;
		if(this.viewing.currentForm.type==="category") {
			name = this.viewing.currentForm.category=="ALL"? 
				this.app.translateText("lbl_all_subscriptions"): 
				this.viewing.currentForm.category;
		} else {
			let feedId = this.viewing.currentForm.feed;
			let cache = this.viewing.subscriptionCache;
			if(cache.map.hasOwnProperty(feedId) && cache.map[feedId].loaded) {
				name = cache.map[feedId].response.value.title;
			} else {
				name = feedId;
			}
		}
		let message = {key: text, arguments: [name]};
		this.app.translateMessage(message);
		dhtmlx.confirm({
			title: this.app.translateText("btn_mark_all_read", [name]),
			type:"confirm",
			text: message.text,
			ok: this.app.translateText("btn_yes"),
			cancel: this.app.translateText("btn_no"),
			callback: $.proxy(function(response) {
				if(response) {
					this.markAllAsRead();
				}
			}, this)
		});
	},
	
	renderSubscriptionInfo: function(subscription, jqTarget) {
		if(rvLog) console.log("[MYRSSViewer][renderSubscriptionInfo]");
		let notEmpty = function(data) {
			return data!=null && $.trim(data).length>0;
		};
		
		let tooltip = "";
		if(notEmpty(subscription.title)) {
			tooltip += this.escapeHtml(subscription.title);
		} else {
			tooltip += this.escapeHtml(subscription.url);
		}
		if(notEmpty(subscription.description)) {
			tooltip += "\n"+ this.escapeHtml(subscription.description);
		}
		if(notEmpty(subscription.copyright)) {
			tooltip += "\n"+ this.escapeHtml(subscription.copyright);
		}
		let iconItem = "";
		if(notEmpty(subscription.icon)) {
			iconItem = 
				"<img src='"+ subscription.icon +"' " +
					"style='width: 16px; height: 16px; vertical-align: middle;'>" +
				"</img>";
		} else {
			iconItem = 
				"<i class='fa fa-file-o' style='vertical-align: text-top;'></i>";
		}
		let link = 
			(notEmpty(subscription.link))?
				subscription.link: subscription.url;
		let postText = " "+ subscription.category!=null?
				"<b>&nbsp;&nbsp;["+ this.escapeHtml(subscription.category) +"]</b> ":
					"";
		postText += (notEmpty(subscription.title))? this.escapeHtml(subscription.title): this.escapeHtml(subscription.url);
		let linkItem = 
			"<span title='"+ tooltip +"' style='font-weight: normal;'>"+
				"<a href='"+ link +"' target='_blank'>"+ 
					iconItem +
				"</a>&nbsp;"+
				postText +
			"</span>";
		jqTarget.append(linkItem);
	},
	
	/**
	 * Observes an element, and invokes the specified callback when all its child images have been loaded (or failed to load).
	 * This allows computations to account for the images height.
	 * Invokes the callback immediatly if no image needs to be tracked.
	 */
	callbackWhenLoaded: function(element, callback) {
		if(rvLogFiner) console.log("[MYRSSViewer][callbackWhenLoaded]");
		let callbackContext = {view: this, callback: callback};
		let observerCallback = $.proxy(function(mutationsList, observer) {
			let tracker = {
				total: 0,
				counter: 0,
				observer: this.observer,
				callback: this.callback
			};
			let loaded = $.proxy(function() {
				try {
					this.counter++;
					if(this.counter>=this.total) {
						this.observer.disconnect();
						this.callback();
					}
				} catch(e) {
					debugger;
					console.log(e);
				}
			}, tracker);
			let tracked = false;
			for(let mutation of mutationsList) {
				if(mutation.type=='childList') {
					$(mutation.target).find("img").each(function(index, item) {
						tracked = true;
						tracker.total++;
						$(item).imagesLoaded().always(loaded);
					});
				}
			}
			if(!tracked) {
				this.observer.disconnect();
				this.callback();
			}
		}, callbackContext);
		let observer = new MutationObserver(observerCallback);
		callbackContext.observer = observer;
		observer.observe(element, {
			childList: true,
			subtree: true
		});
	},
	
	makeCount: function(item, payload) {
		if(rvLogFiner) console.log("[MYRSSViewer][makeCount](payload: "+ JSON.stringify(payload) +")");
		item.view = this;
		item.requesting = false;
		this.countValue = null;
		item.payload = payload;
		item.onCountUpdate = null;
		item.recount = $.proxy(function() {
			this.requesting = true;
			this.empty().append("<i class=\"fa fa-spinner fa-pulse fa-fw\"></i>");
			$.ajax({
				method: "POST",
				url: "mvc/viewer/count",
				data: JSON.stringify(this.payload),
				dataType: "json",
				contentType: "application/json",
				success: $.proxy(function(data) {
					this.requesting = false;
					this.empty();
					this.countValue = data;
					if(data!==0) {
						this.append("<b>("+ data +")</b>");
					}
					if(this.onCountUpdate!=null) {
						this.onCountUpdate();
					}
				}, this),
				error: $.proxy(function(xhr) {
					if(this.view.app.processErrorResponse(xhr)) {
						this.requesting = false;
						this.empty().append("<i class=\"fa fa-exclamation-circle\"></i>");
					}
				}, this)
			});
		}, item);
	},
	
	escapeHtml: function(unsafe) {
		if(rvLogFiner) console.log("[MYRSSViewer][escapeHtml]");
		return unsafe
			.replace(/&/g, "&amp;")
			.replace(/</g, "&lt;")
			.replace(/>/g, "&gt;")
			.replace(/"/g, "&quot;")
			.replace(/'/g, "&#039;");
	},
};

for(let name in viewerMethods) {
	MYRSSViewer.prototype[name] = viewerMethods[name];
}

/**
 * A relation of counters based on identifier values.
 */
function CounterMap() {
	if(cmLog) console.log("[CounterMap]");
	this.data = {
		categories: [],
		categoryCounters: {},
		feeds: [],
		feedCounters: {}
	};
}

let counterMapMethods = {
	
	addCounter: function(identifier, counter) {
		if(cmLog) console.log("[addCounter] for identifier: "+ JSON.stringify(identifier));
		if(this._validateId(identifier)) {
			if("category"==identifier.type) {
				if(!this.data.categories.includes(identifier.category)) {
					this.data.categories.push(identifier.category);
					this.data.categoryCounters[identifier.category] = [];
				}
				this.data.categoryCounters[identifier.category].push(counter);
			} else {
				if(!this.data.feeds.includes(identifier.feed)) {
					this.data.feeds.push(identifier.feed);
					this.data.feedCounters[identifier.feed] = [];
				}
				this.data.feedCounters[identifier.feed].push(counter);
			}
		}
	},
	
	getCounters: function(identifier) {
		if(cmLog) console.log("[getCounters] for identifier: "+ JSON.stringify(identifier));
		let result = [];
		if(this._validateId(identifier)) {
			if("category"==identifier.type) {
				if(this.data.categories.includes(identifier.category)) {
					result = this.data.categoryCounters[identifier.category].slice();
				}
			} else {
				if(this.data.feeds.includes(identifier.feed)) {
					result = this.data.feedCounters[identifier.feed].slice();
				}
			}
		}
		return result;
	},
	
	_validateId: function(identifier) {
		if(arguments.length==0 || identifier==undefined || identifier==null) {
			return false;
		}
		if(!identifier.hasOwnProperty("type") || ["category", "feed"].indexOf(identifier.type)<0) {
			return false;
		}
		if("category"===identifier.type && (!identifier.hasOwnProperty("category") || identifier.category==undefined || identifier.category==null)) {
			return false;
		}
		if("feed"===identifier.type && (!identifier.hasOwnProperty("feed") || identifier.feed==undefined || identifier.feed==null)) {
			return false;
		}
		return true;
	}
};

for(let name in counterMapMethods) {
	CounterMap.prototype[name] = counterMapMethods[name];
}

