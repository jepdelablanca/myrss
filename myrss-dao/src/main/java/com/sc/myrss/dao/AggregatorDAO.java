package com.sc.myrss.dao;

import java.util.List;

import com.sc.myrss.model.rss.Article;
import com.sc.myrss.model.rss.Subscription;
import com.sc.myrss.model.security.User;

public interface AggregatorDAO {

	public List<User> listBrowserUsers();

	public void updateSubscription(Subscription subscription);

	public boolean articleExists(Article article);

	public void addArticle(Article article);

	public Long purgeSubscription(Subscription subscription, Long guardTime);

}
