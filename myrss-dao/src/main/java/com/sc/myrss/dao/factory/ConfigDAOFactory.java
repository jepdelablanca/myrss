package com.sc.myrss.dao.factory;

import com.sc.myrss.dao.ConfigDAO;

public interface ConfigDAOFactory {

	public ConfigDAO getDAO();

}
