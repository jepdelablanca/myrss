package com.sc.myrss.dao.factory;

import com.sc.myrss.dao.SecurityDAO;

public interface SecurityDAOFactory {

	public SecurityDAO getDAO();

}
