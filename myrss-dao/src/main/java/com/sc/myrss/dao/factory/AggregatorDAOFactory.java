package com.sc.myrss.dao.factory;

import com.sc.myrss.dao.AggregatorDAO;

public interface AggregatorDAOFactory {

	public AggregatorDAO getDAO();

}
