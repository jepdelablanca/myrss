package com.sc.myrss.dao;

import java.util.List;

import com.sc.myrss.model.security.Role;
import com.sc.myrss.model.security.SecResource;
import com.sc.myrss.model.security.User;

public interface UserManagementDAO {

	public List<SecResource> listSecResources();

	public void addResourceToRole(String resourceId, String roleId);

	public void removeResourcesFromRole(List<String> resourceIds, String roleId);

	public void setRoleResources(String roleId, List<String> resourceIds);

	public void saveRole(Role role);

	public boolean isRoleInUse(String roleId);

	public void deleteRole(String roleId);

	public void saveUser(User user);

	public void deleteUser(String userId);

}
