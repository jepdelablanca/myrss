package com.sc.myrss.model.rss;

public enum SyndicationType {

	RSS2, ATOM;

}
