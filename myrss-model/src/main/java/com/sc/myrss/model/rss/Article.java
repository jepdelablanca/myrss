package com.sc.myrss.model.rss;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Article implements Serializable {

	private static final long serialVersionUID = 383477008646447145L;

	private String userId;

	private String subscriptionUrl;

	private String title;

	private String contentHash;

	private Long readId;

	private String content;

	private String authors;

	private String categories;

	private String guid;

	private Date publicationDate;

	private String sourceUrl;

	private boolean read;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSubscriptionUrl() {
		return subscriptionUrl;
	}

	public void setSubscriptionUrl(String subscriptionUrl) {
		this.subscriptionUrl = subscriptionUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContentHash() {
		return contentHash;
	}

	public void setContentHash(String contentHash) {
		this.contentHash = contentHash;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAuthors() {
		return authors;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	public String getCategories() {
		return categories;
	}

	public void setCategories(String categories) {
		this.categories = categories;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public Long getReadId() {
		return readId;
	}

	public void setReadId(Long readId) {
		this.readId = readId;
	}

	@Override
	public String toString() {
		return "Article [userId=" + userId + ", subscriptionUrl=" + subscriptionUrl + ", title=" + title
				+ ", contentHash=" + contentHash + ", readId=" + readId + ", content=" + content + ", authors="
				+ authors + ", categories=" + categories + ", guid=" + guid + ", publicationDate=" + publicationDate
				+ ", sourceUrl=" + sourceUrl + ", read=" + read + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(contentHash, subscriptionUrl, title, userId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Article))
			return false;
		Article other = (Article) obj;
		return Objects.equals(contentHash, other.contentHash) && Objects.equals(subscriptionUrl, other.subscriptionUrl)
				&& Objects.equals(title, other.title) && Objects.equals(userId, other.userId);
	}

}
