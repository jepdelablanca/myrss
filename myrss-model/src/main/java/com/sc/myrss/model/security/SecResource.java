package com.sc.myrss.model.security;

import java.io.Serializable;

public class SecResource implements Serializable {

	private static final long serialVersionUID = -2864261259988136715L;

	private String id;

	private String name;

	private String parentId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@Override
	public String toString() {
		return "SecResource [id=" + id + ", name=" + name + ", parentId=" + parentId + "]";
	}

}
