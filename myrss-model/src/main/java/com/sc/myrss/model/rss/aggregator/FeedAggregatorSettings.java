package com.sc.myrss.model.rss.aggregator;

public class FeedAggregatorSettings extends UserAggregatorSettings {

	public FeedAggregatorSettings() {}

	public FeedAggregatorSettings(String userId, String url,
			String fetchRateCronExpression, Long readItemsGuardTime) {
		this.setUserId(userId);
		this.url = url;
		this.setFetchRateCronExpression(fetchRateCronExpression);
		this.setReadItemsGuardTime(readItemsGuardTime);
	}

	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
