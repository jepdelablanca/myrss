# MYRSS

MYRSS is a java RSS feed aggregator.

Note that this is a work in progress, and there is a lot missing yet, but it works.

## Prerequisites

You will need an SQL database and a Java web server. I used [PostgreSQL](https://www.postgresql.org/) and [Apache Tomcat](http://tomcat.apache.org/).

Also, you will need to compile the Java web application. I use the [Eclipse](https://www.eclipse.org) IDE for that.

## Installation

First of all, clone or download the source code.

Install the PostgreSQL database, and the Apache Tomcat web server. Follow the instructions provided at their own sites.

Create the database user for the application. From a Linux commandline:

```bash
sudo -u postgres psql
postgres=# create database myrss;
postgres=# create user myrss with encrypted password 'myrss';
postgres=# grant all privileges on database myrss to myrss;
```

Edit the context.xml file in the Tomcat config folder, and add the database Resource element:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Context>
    <Resource
        name="jdbc/myrss"
        auth="container"
        type="javax.sql.DataSource"
        username="myrss"
        password="myrss"
        driverClassName="org.posrgresql.Driver"
        url="jdbc:postgresql://localhost:5432/MYRSS"
        maxActive="8"
        maxIdle="4" />
</Context>
```

Run the scripts in the scripts\postgresql folder in order to create the schema and provide a basic configuration. Run first the 01-model.sql file and then the 02-data.sql file.

Load the project in Eclipse, and compile with maven:clean and maven:install. You will get a deployable application file in the target folder, named myrss.war. Copy it to the Tomcat webapps folder.

## Usage

User administration is still undone. In order to login to the application, you will need to create a user account in the users table.

To create a password, run the myrss-cmd/src/main/java/com/sc/myrss/cmd/Main class, passing the desired password as argument. Store the provided output in the password_hash field of the users table.

Subscription administration is undone too. Fill the subscriptions table with your list of RSS or ATOM feeds. You only need to provide the username and url fields, the application will fetch the rest of the data.

Now you hace a user and a list of feeds, you are ready to use the application. From your web browser, open the address http://localhost/myrss. Login with your credentials, and enjoy your reading!

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/)