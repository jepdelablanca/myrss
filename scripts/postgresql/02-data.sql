INSERT INTO "myrss"."roles" (id,name) VALUES ('superadmin', 'SuperAdmin');
INSERT INTO "myrss"."roles" (id,name) VALUES ('admin', 'Admin');
INSERT INTO "myrss"."roles" (id,name) VALUES ('user', 'User');

INSERT INTO "myrss"."sec_resources" (id,name) VALUES ('access', 'Access the application');
INSERT INTO "myrss"."sec_resources" (id,name,parent_id) VALUES ('admin', 'Manage the application', 'access');
INSERT INTO "myrss"."sec_resources" (id,name,parent_id) VALUES ('my_profile', 'Configure your profile', 'access');
INSERT INTO "myrss"."sec_resources" (id,name,parent_id) VALUES ('browse', 'Browse your feeds', 'access');

INSERT INTO "myrss"."roles_sec_resources" (role_id,sec_resource_id) VALUES ('superadmin', 'access');
INSERT INTO "myrss"."roles_sec_resources" (role_id,sec_resource_id) VALUES ('superadmin','admin');

INSERT INTO "myrss"."roles_sec_resources" (role_id,sec_resource_id) VALUES ('admin', 'access');
INSERT INTO "myrss"."roles_sec_resources" (role_id,sec_resource_id) VALUES ('admin','admin');
INSERT INTO "myrss"."roles_sec_resources" (role_id,sec_resource_id) VALUES ('admin','my_profile');
INSERT INTO "myrss"."roles_sec_resources" (role_id,sec_resource_id) VALUES ('admin','browse');

INSERT INTO "myrss"."roles_sec_resources" (role_id,sec_resource_id) VALUES ('user', 'access');
INSERT INTO "myrss"."roles_sec_resources" (role_id,sec_resource_id) VALUES ('user','my_profile');
INSERT INTO "myrss"."roles_sec_resources" (role_id,sec_resource_id) VALUES ('user','browse');

INSERT INTO "myrss"."users" (login,role_id,name) VALUES ('root', 'superadmin', 'Root');
INSERT INTO "myrss"."users" (login,role_id,name) VALUES ('admin', 'admin', 'Admin');
INSERT INTO "myrss"."users" (login,role_id,name) VALUES ('user', 'user', 'User');

INSERT INTO "myrss"."aggregator_settings" (user_id,subscription_url,fetch_rate_cron_expression,read_items_guard_time) 
VALUES ('SYSTEM', 'ALL', '0 */60 * ? * *', 2629800000);

INSERT INTO "myrss"."aggregator_settings" (user_id,subscription_url,fetch_rate_cron_expression,read_items_guard_time) 
VALUES ('user', 'ALL', '0 */30 * ? * *', 2629800000);

