package com.sc.myrss.neg.rss.downloader;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sc.myrss.model.rss.Article;
import com.sc.myrss.model.rss.Subscription;
import com.sc.myrss.model.rss.SyndicationType;
import com.sc.myrss.neg.rss.downloader.exception.BootException;
import com.sc.myrss.neg.rss.downloader.exception.ParseException;
import com.sc.myrss.reader.atom.vo.CategoryType;
import com.sc.myrss.reader.atom.vo.ContentType;
import com.sc.myrss.reader.atom.vo.DateTimeType;
import com.sc.myrss.reader.atom.vo.EntryType;
import com.sc.myrss.reader.atom.vo.FeedType;
import com.sc.myrss.reader.atom.vo.GeneratorType;
import com.sc.myrss.reader.atom.vo.IdType;
import com.sc.myrss.reader.atom.vo.LinkType;
import com.sc.myrss.reader.atom.vo.PersonType;
import com.sc.myrss.reader.atom.vo.TextType;
import com.sc.myrss.reader.rss20.vo.Category;
import com.sc.myrss.reader.rss20.vo.Guid;
import com.sc.myrss.reader.rss20.vo.Image;
import com.sc.myrss.reader.rss20.vo.Rss;
import com.sc.myrss.reader.rss20.vo.RssChannel;
import com.sc.myrss.reader.rss20.vo.RssItem;

public class FeedDownloaderImpl implements FeedDownloader {

	private static final Logger log = Logger.getLogger(FeedDownloaderImpl.class);

	private static final DateFormat RFC822_DATE_FORMAT = 
			new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");

	private static final String ATOM_XSD_FILE = "/org/w3/atom.xsd";

	private static final String RSS20_XSD_FILE = "/org/w3/rss20.xsd";

	private HttpClient httpClient = null;

	private Validator atomValidator = null;

	private Validator rss20Validator = null;

	private Unmarshaller atomUnmarshaller = null;

	private Unmarshaller rss20Unmarshaller = null;

	FeedDownloaderImpl() {
		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		try {
			Schema rss20Schema = schemaFactory.newSchema(getClass().getResource(RSS20_XSD_FILE));
			rss20Validator = rss20Schema.newValidator();
		} catch (SAXException e) {
			throw new BootException("Error preparing RSS20 validator: "+ e.getMessage(), e);
		}
		try {
			Schema atomSchema = schemaFactory.newSchema(getClass().getResource(ATOM_XSD_FILE));
			atomValidator = atomSchema.newValidator();
		} catch (SAXException e) {
			throw new BootException("Error preparing ATOM validator: "+ e.getMessage(), e);
		}
		try {
			JAXBContext rss20JaxbContext = JAXBContext.newInstance(Rss.class);
			rss20Unmarshaller = rss20JaxbContext.createUnmarshaller();
		} catch (JAXBException e) {
			throw new BootException("Error preparing RSS20 unmarshaller: "+ e.getMessage(), e);
		}
		try {
			JAXBContext atomFeedJaxbContext = JAXBContext.newInstance(FeedType.class);
			atomUnmarshaller = atomFeedJaxbContext.createUnmarshaller();
		} catch (JAXBException e) {
			throw new BootException("Error preparing ATOM unmarshaller: "+ e.getMessage(), e);
		}
		httpClient = HttpClientBuilder.create().setRedirectStrategy(new LaxRedirectStrategy()).build();
	}

	@Override
	public Subscription downloadSubscription(String address, 
			SyndicationType hint) throws IOException, ParseException {
		Subscription result = null;
		long start = System.currentTimeMillis();
		String pageText = fetchUrl(address);
		long mark = System.currentTimeMillis();
		if(log.isDebugEnabled()) log.debug("[downloadSubscription] Download completed in "+ (mark - start) +"ms");
		SyndicationType type = hint==null? checkFeedType(pageText): hint;
		switch(type) {
			case RSS2: {
				result = extractFromRss(pageText);
			} break;
			case ATOM: {
				result = extractFromAtom(pageText);
			}
		}
		if(log.isDebugEnabled()) log.debug("[downloadSubscription] Subscription parsed in "+ (System.currentTimeMillis() - mark) +"ms");
		result.setSyndicationTpye(type);
		return result;
	}

	private String fetchUrl(String address) throws IOException {
		HttpGet httpGet = null;
		try {
			httpGet = new HttpGet(address);
			HttpResponse response = httpClient.execute(httpGet);
			return new EncodingAwareStringResponseHandler().handleResponse(response);
		} catch(Throwable t) {
			throw new IOException("Error fetching contents", t);
		} finally {
			if(httpGet!=null) {
				httpGet.releaseConnection();
			}
		}
	}

	private SyndicationType checkFeedType(String rawXmlSource) throws ParseException {
		try {
			rss20Validator.validate(new StreamSource(new StringReader(rawXmlSource)));
			return SyndicationType.RSS2;
		} catch(Exception e) {}
		try {
			atomValidator.validate(new StreamSource(new StringReader(rawXmlSource)));
			return SyndicationType.ATOM;
		} catch(Exception e) {}
		ByteArrayInputStream bais = new ByteArrayInputStream(rawXmlSource.getBytes());
		try {
			rss20Unmarshaller.unmarshal(new InputSource(bais));
			return SyndicationType.RSS2;
		} catch(Exception e) {}
		bais.reset();
		try {
			atomUnmarshaller.unmarshal(new InputSource(bais));
			return SyndicationType.ATOM;
		} catch(Exception e) {
			throw new ParseException("Unknown syndication type");
		}
	}

	private Subscription extractFromRss(String text) throws ParseException {
		try {
			return parseRss((Rss)rss20Unmarshaller.unmarshal(
				new InputSource(new ByteArrayInputStream(text.getBytes())))); // InputSource should autodetect the text encoding
		} catch(Throwable t) {
//			log.debug("Read text: \n\n\n"+ text +"\n\n\n");
			throw new ParseException("Error parsing RSS content", t);
		}
	}

	private Subscription extractFromAtom(String text) throws ParseException {
		try {
			return parseAtom((FeedType)((JAXBElement<?>)atomUnmarshaller.unmarshal(
					new InputSource(new ByteArrayInputStream(text.getBytes())))).getValue());
		} catch(Throwable t) {
			throw new ParseException("Error parsing ATOM content", t);
		}
	}

	private Subscription parseRss(Rss rss) {
		Subscription result = new Subscription();
		if(rss.getChannel()!=null) {
			RssChannel channel = rss.getChannel();
			for(Object item : channel.getTitleOrLinkOrDescription()) {
				if(item instanceof JAXBElement<?>) {
					JAXBElement<?> element = (JAXBElement<?>)item;
					switch(element.getName().getLocalPart()) {
						case "title": result.setTitle(makePlain(element.getValue())); break;
						case "description": result.setDescription(makePlain(element.getValue())); break;
						case "link": result.setLink(makePlain(element.getValue())); break;
						case "image": {
							Image image = (Image)element.getValue();
							result.setIcon(makePlain(image==null? null: image.getUrl()));
						} break;
						case "category": result.setCategories(makePlain(element.getValue())); break;
						case "copyright": result.setCopyright(makePlain(element.getValue())); break;
						case "generator": result.setGenerator(makePlain(element.getValue())); break;
						default:
					}
				}
			}
			if(channel.getItem()!=null) {
				result.setArticles(new ArrayList<Article>());
				for(RssItem rssItem : channel.getItem()) {
					if(rssItem.getTitleOrDescriptionOrLink()!=null) {
						Article article = new Article();
						for(Object item : rssItem.getTitleOrDescriptionOrLink()) {
							if(item instanceof JAXBElement<?>) {
								JAXBElement<?> element = (JAXBElement<?>)item;
								switch(element.getName().getLocalPart()) {
								case "title": article.setTitle(makePlain(element.getValue())); break;
								case "link": article.setSourceUrl(makePlain(element.getValue())); break;
								case "description": article.setContent((String)element.getValue()); break;
								case "author": article.setAuthors(makePlain(element.getValue())); break;
								case "category": {
									Category category = (Category)element.getValue();
									article.setCategories(makePlain(category==null? null: category.getValue()));
								} break;
								case "guid": {
									Guid guid = (Guid)element.getValue();
									article.setGuid(makePlain(guid==null? null: guid.getValue()));
								} break;
								case "pubDate": try {
									article.setPublicationDate(RFC822_DATE_FORMAT.parse((String)element.getValue()));
								} catch (java.text.ParseException e) {} break;
								default:
								}
							}
						}
						result.getArticles().add(article);
					}
				}
			}
		}
		return result;
	}

	private Subscription parseAtom(FeedType data) {
		Subscription result = new Subscription();
		result.setArticles(new ArrayList<Article>());
		StringBuilder categories = new StringBuilder();
		for(Object object : data.getAuthorOrCategoryOrContributor()) {
			if(object instanceof JAXBElement<?>) {
				JAXBElement<?> item = (JAXBElement<?>) object;
				switch(item.getName().getLocalPart()) {
					case "category": {
						CategoryType categoryType = (CategoryType)item.getValue();
						if(categoryType!=null) {
							if(categories.length()>0) 
								categories.append(",");
							categories.append(makePlain(categoryType.getTerm()));
						}
					} break;
					case "title": result.setTitle(makePlain(getText((TextType)item.getValue()))); break;
					case "subtitle": result.setDescription(makePlain(getText((TextType)item.getValue()))); break;
					case "link": {
						LinkType link = (LinkType)item.getValue();
						if("text/html".equals(link.getType())) {
							result.setLink(link.getHref());
						}
					} break;
					case "generator": {
						GeneratorType generatorType = (GeneratorType)item.getValue();
						result.setGenerator(makePlain(generatorType==null? null: generatorType.getValue())); break;
					}
					case "entry": result.getArticles().add(parseArticle((EntryType)item.getValue())); break;
					case "rights": result.setCopyright(makePlain(getText((TextType)item.getValue()))); break;
					case "icon": {
						// IconType icon = (IconType) item.getValue();
						throw new UnsupportedOperationException("Unimplemented IconType when parsing "
							+ "ATOM feeds, please stop everything and correct this ASAP");
					}
					default:
				}
			}
		}
		if(categories.length()>0) {
			result.setCategories(categories.toString());
		}
		return result;
	}

	private Article parseArticle(EntryType entry) {
		Article result = new Article();
		StringBuilder categories = new StringBuilder();
		for(Object object : entry.getAuthorOrCategoryOrContent()) {
			if(object instanceof JAXBElement<?>) {
				JAXBElement<?> item = (JAXBElement<?>)object;
				String name = item.getName().getLocalPart();
				switch(name) {
					case "published": {
						DateTimeType value = (DateTimeType) item.getValue();
						Date dateValue = value.getValue().toGregorianCalendar().getTime();
						 result.setPublicationDate(dateValue);
					} break;
					case "category": {
						if(categories.length()>0) 
							categories.append(",");
						categories.append(makePlain(((CategoryType)item.getValue()).getTerm()));
					} break;
					case "author": {
						for(Object attr : ((PersonType)item.getValue()).getNameOrUriOrEmail()) {
							if(attr instanceof JAXBElement<?>) {
								JAXBElement<?> attrItem = (JAXBElement<?>) attr;
								if("name".contentEquals(attrItem.getName().getLocalPart())) {
									result.setAuthors(makePlain((String)attrItem.getValue()));
								}
							}
						}
					} break;
					case "content": result.setContent(getContent((ContentType) item.getValue())); break;
					case "title": result.setTitle(makePlain(getText((TextType)item.getValue()))); break;
					case "guid": result.setGuid(makePlain(((IdType)item.getValue()).getValue())); break;
					case "link": {
						LinkType link = (LinkType)item.getValue();
						if("text/html".equals(link.getType())) {
							result.setSourceUrl(link.getHref());
						}
					}
					default:
				}
			}
		}
		if(categories.length()>0) {
			result.setCategories(categories.toString());
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	private String getText(TextType element) {
		StringBuilder result = new StringBuilder();
		if(element!=null) {
			for(String row : (ArrayList<String>)(ArrayList<?>)element.getContent()) {
				result.append(row);
			}
		}
		return result.toString();
	}

	@SuppressWarnings("unchecked")
	private String getContent(ContentType element) {
		StringBuilder result = new StringBuilder();
		for(String row : (ArrayList<String>)(ArrayList<?>)element.getContent()) {
			result.append(row);
		}
		return result.toString();
	}

	private String makePlain(Object source) {
		return source==null? null: ((String)source).replaceAll("\\<.*?\\>", "").replaceAll("\\n", " ").trim();
	}
}
