package com.sc.myrss.neg.security;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.sc.myrss.dao.factory.SecurityDAOFactory;
import com.sc.myrss.model.security.User;

@Service
public class SecurityServiceImpl implements SecurityService {

	@Autowired
	private SecurityDAOFactory securityDAOFactory;

	@Override
	@Transactional
	public boolean existsUser(String login) {
		return securityDAOFactory.getDAO().existsUser(login);
	}

	@Override
	@Transactional
	public User getUser(String login) {
		return securityDAOFactory.getDAO().loadUser(login);
	}

	@Override
	public String generateHash(String password) {
		return BCrypt.hashpw(password, BCrypt.gensalt());
	}

	@Override
	@Transactional
	public boolean validateLogin(String login, String password) {
		return existsUser(login)? 
			BCrypt.checkpw(password, securityDAOFactory.getDAO().loadUser(login).getHash()): 
			false;
	}
}
