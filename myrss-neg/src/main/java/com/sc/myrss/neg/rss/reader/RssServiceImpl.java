package com.sc.myrss.neg.rss.reader;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sc.myrss.dao.factory.RssDAOFactory;
import com.sc.myrss.model.rss.Article;
import com.sc.myrss.model.rss.Subscription;

@Service
public class RssServiceImpl implements RssService {

	@Autowired
	RssDAOFactory rssDAOFactory;

	@Override
	@Transactional
	public List<Subscription> getSubscriptions(String login) {
		return rssDAOFactory.getDAO().getUsersSubscriptions(login);
	}

	@Override
	@Transactional
	public Subscription getSubscription(String userId, String subscriptionUrl) {
		return rssDAOFactory.getDAO().getSubscription(userId, subscriptionUrl);
	}

	@Override
	@Transactional
	public List<Long> getSubscriptionArticleIds(String userId, String subscriptionUrl, boolean unreadOnly) {
		return rssDAOFactory.getDAO().getSubscriptionArticleIds(userId, subscriptionUrl, unreadOnly);
	}

	@Override
	@Transactional
	public List<Long> getCategoryArticleIds(String userId, String categoryName, boolean unreadOnly) {
		return categoryName==null?
			rssDAOFactory.getDAO().getUserArticleIds(userId, unreadOnly):
			rssDAOFactory.getDAO().getCategoryArticleIds(userId, categoryName, unreadOnly);
	}

	@Override
	@Transactional
	public List<Article> getArticlesByIds(List<Long> ids) {
		return rssDAOFactory.getDAO().getArticlesByIds(ids);
	}

	@Override
	@Transactional
	public Integer getSubscriptionUnreadCount(String userId, String subscriptionUrl) {
		return rssDAOFactory.getDAO().countSubscriptionPendingArticles(userId, subscriptionUrl);
	}

	@Override
	@Transactional
	public Integer getCategoryUnreadCount(String userId, String categoryName) {
		return categoryName==null?
			rssDAOFactory.getDAO().countPendingArticles(userId):
			rssDAOFactory.getDAO().countCategoryPendingArticles(userId, categoryName);
	}
	@Override
	@Transactional
	public void markArticleAsRead(Long readId, boolean readValue) {
		rssDAOFactory.getDAO().markArticleAsRead(readId, readValue);
	}

	@Override
	@Transactional
	public void markSubscriptionAsRead(String userId, String subscriptionUrl, boolean readValue) {
		rssDAOFactory.getDAO().markSubscriptionAsRead(userId, subscriptionUrl, readValue);
	}

	@Override
	@Transactional
	public void markCategoryAsRead(String userId, String categoryName, boolean readValue) {
		if(categoryName==null) {
			rssDAOFactory.getDAO().markUserAsRead(userId, readValue);
		} else {
			rssDAOFactory.getDAO().markCategoryAsRead(userId, categoryName, readValue);
		}
	}

}
