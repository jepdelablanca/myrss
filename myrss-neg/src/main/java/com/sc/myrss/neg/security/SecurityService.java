package com.sc.myrss.neg.security;

import com.sc.myrss.model.security.User;

public interface SecurityService {

	public boolean existsUser(String name);

	public User getUser(String login);

	public String generateHash(String password);

	public boolean validateLogin(String login, String password);
}
