package com.sc.myrss.neg.rss.downloader.exception;

public class ParseException extends Exception {

	private static final long serialVersionUID = -2666167325924383153L;

	public ParseException(String message) {
		super(message);
	}

	public ParseException(String message, Throwable cause) {
		super(message);
		initCause(cause);
	}

}
