package com.sc.myrss.neg.rss.downloader;

import java.io.IOException;

import com.sc.myrss.model.rss.Subscription;
import com.sc.myrss.model.rss.SyndicationType;
import com.sc.myrss.neg.rss.downloader.exception.ParseException;

public interface FeedDownloader {

	/**
	 * This download method just fetches data from the source. It does not set
	 * the primary key attributes for subscriptions and articles, nor content hashes
	 * or status flags.
	 * 
	 * Also, no length restrictions are applied: the retrieved data can 
	 * exceed database limitations.
	 * 
	 * @param hint helps avoid unnecesary and heavy-duty syndication type comprobation.
	 * This parameter is optional, but recomended.
	 */
	public Subscription downloadSubscription(String address, 
			SyndicationType hint) throws IOException, ParseException;

}
