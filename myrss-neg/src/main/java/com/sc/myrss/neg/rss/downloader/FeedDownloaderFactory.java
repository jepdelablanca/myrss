package com.sc.myrss.neg.rss.downloader;

import java.util.HashMap;
import java.util.Map;

public class FeedDownloaderFactory {

	private static final Map<String, FeedDownloader> instances =
			new HashMap<String, FeedDownloader>();

	public static FeedDownloader getDownloader() {
		String downloaderId = Thread.currentThread().getName();
		if(!instances.containsKey(downloaderId)) {
			instances.put(downloaderId, new FeedDownloaderImpl());
		}
		return instances.get(downloaderId);
	}

	public void freeDownloader(String key) {
		if(instances.containsKey(key)) {
			instances.remove(key);
		}
	}

}
