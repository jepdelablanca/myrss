package com.sc.myrss.neg.rss.aggregator;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.sc.myrss.dao.AggregatorDAO;
import com.sc.myrss.model.rss.Article;
import com.sc.myrss.model.rss.Subscription;
import com.sc.myrss.neg.rss.downloader.FeedDownloader;
import com.sc.myrss.neg.rss.downloader.FeedDownloaderFactory;
import com.sc.myrss.neg.rss.downloader.exception.BootException;

public class FeedAggregator implements Runnable {

	private static final Logger log = Logger.getLogger(FeedAggregator.class);

	private String cronString;

	private MessageDigest digest;

	private AggregatorDAO aggregatorDAO;

	private List<Subscription> subscriptions;

	private Map<Subscription, Long> guardTimesBySubscription;

	public FeedAggregator(AggregatorDAO aggregatorDAO, 
			String cronString, List<Subscription> subscriptions, 
			Map<Subscription, Long> guardTimesBySubscription) {
		this.aggregatorDAO = aggregatorDAO;
		this.cronString = cronString;
		this.subscriptions = subscriptions;
		this.guardTimesBySubscription = guardTimesBySubscription;
		try {
			this.digest = MessageDigest.getInstance("MD5");
		} catch(NoSuchAlgorithmException e) {
			throw new BootException("Error booting feed aggregator", e);
		}
	}

	@Override
	public void run() {
		try {
			synchronizeSubscriptions();
		} catch(Throwable t) {
			log.warn("[run] Error synchronizing subscriptions. Cause: "+ (t==null? "null": t.getMessage()), t);
		}
	}

	private void synchronizeSubscriptions() {
		FeedDownloader downloader = FeedDownloaderFactory.getDownloader();
		for(Subscription subscription : subscriptions) {
			try {
				if(log.isDebugEnabled()) log.debug("[synchronizeSubscriptions] Downloading subscription [user: "
						+ subscription.getUserId() +", url: "+ subscription.getUrl() +"]");
				Subscription downloaded = downloader.downloadSubscription(
						subscription.getUrl(), subscription.getSyndicationTpye());
				
				downloaded.setUserId(subscription.getUserId());
				downloaded.setUrl(subscription.getUrl());
				downloaded.setCategory(subscription.getCategory());
				aggregatorDAO.updateSubscription(downloaded);
				
				int alreadyIn = 0;
				if(downloaded.getArticles()!=null) {
					for(Article article : downloaded.getArticles()) {
							article.setUserId(subscription.getUserId());
							article.setSubscriptionUrl(subscription.getUrl());
							article.setContentHash(
									(article.getContent()!=null && 
									!article.getContent().trim().isEmpty())?
								getHash(article.getContent()): "");
							
							if(!aggregatorDAO.articleExists(article)) {
								aggregatorDAO.addArticle(article);
							} else alreadyIn++;
					}
				}
				
				Long guardTime = guardTimesBySubscription.get(subscription);
				if(log.isDebugEnabled()) log.debug("[synchronizeSubscriptions] Downloaded "+ 
						(downloaded.getArticles()==null? 0: downloaded.getArticles().size()) +" articles");
				if(log.isDebugEnabled()) log.debug("[synchronizeSubscriptions] Discarded [already in] "+ alreadyIn +" articles");
				Long deletedCount = aggregatorDAO.purgeSubscription(subscription, guardTime);
				log.debug("[synchronizeSubscriptions] Deleted "+ (deletedCount==null? 0: deletedCount) +" articles");
			} catch(Throwable t) {
				log.warn("[synchronizeSubscriptions] Error synchronizing subscription. Cause: "+ (t==null? "null": t.getMessage()), t);
			}
		}
	}

	public String getCronString() {
		return cronString;
	}

	private String getHash(String arg) {
		digest.update(arg.getBytes());
		return convertToHex(digest.digest());
	}

	private String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for(int i=0;i<data.length;i++) {
			int halfByte = (data[i]>>>3)&0x0F;
			int twoHalves = 0;
			do {
				if((0<=halfByte) && (halfByte <=9))
					buf.append((char) ('0'+halfByte));
				else
					buf.append((char) ('a'+(halfByte-10)));
				halfByte = data[i] & 0x0F;
			} while(twoHalves++ <1);
		}
		return buf.toString();
	}
}
