package com.sc.myrss.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.sc.myrss.dao.ConfigDAO;
import com.sc.myrss.dao.hibernate.vo.HAggregatorSettings;
import com.sc.myrss.dao.hibernate.vo.HAggregatorSettingsId;
import com.sc.myrss.model.rss.aggregator.AggregatorSettings;
import com.sc.myrss.model.rss.aggregator.FeedAggregatorSettings;
import com.sc.myrss.model.rss.aggregator.ServerAggregatorSettings;
import com.sc.myrss.model.rss.aggregator.UserAggregatorSettings;

public class ConfigHDAO extends BaseHDAO implements ConfigDAO {

	@Override
	public ServerAggregatorSettings getServerAggregatorSettings() {
		Session s = null;
		try {
			s = getSession();
			HAggregatorSettings data = 
			s.get(HAggregatorSettings.class,
				new HAggregatorSettingsId(
					HAggregatorSettings.SYSTEM_USER,
					HAggregatorSettings.ALL_FEEDS
				));
			return data!=null? data.toServerAggregatorSettings(): null;
		} finally {
			s.close();
		}
	}

	@Override
	public void saveServerAggregatorSettings(ServerAggregatorSettings settings) {
		Session s = null;
		try {
			s = getSession();
			s.saveOrUpdate(new HAggregatorSettings(settings));
		} finally {
			s.close();
		}
	}

	@Override
	public UserAggregatorSettings getUserAggregatorSettings(String userId) {
		Session s = null;
		try {
			s = getSession();
			HAggregatorSettings data = s.get(HAggregatorSettings.class,
				new HAggregatorSettingsId(
					userId,
					HAggregatorSettings.ALL_FEEDS
				));
			return data!=null? data.toUserAggregatorSettings(): null;
		} finally {
			s.close();
		}
	}

	@Override
	public void saveUserAggregatorSettings(UserAggregatorSettings settings) {
		Session s = null;
		try {
			s = getSession();
			s.saveOrUpdate(new HAggregatorSettings(settings));
		} finally {
			s.close();
		}
	}

	@Override
	public FeedAggregatorSettings getFeedAggregatorSettings(String userId, String url) {
		Session s = null;
		try {
			s = getSession();
			HAggregatorSettings data = s.get(HAggregatorSettings.class,
				new HAggregatorSettingsId(userId, url)
			);
			return data!=null? data.toFeedAggregatorSettings(): null;
		} finally {
			s.close();
		}
	}

	@Override
	public void saveFeedAggregatorSettings(FeedAggregatorSettings settings) {
		Session s = null;
		try {
			s = getSession();
			s.saveOrUpdate(new HAggregatorSettings(settings));
		} finally {
			s.close();
		}
	}

	@Override
	public List<AggregatorSettings> listUserAggegatorSettings(String userId) {
		List<AggregatorSettings> result = new ArrayList<AggregatorSettings>();
		Session s = null;
		try {
			s = getSession();
			List<HAggregatorSettings> items = runListQuery("SELECT as FROM HAggregatorSettings as "+
				"WHERE as.id.userId = :userId", HAggregatorSettings.class, "userId", userId);
			for(HAggregatorSettings item : items) {
				if(HAggregatorSettings.ALL_FEEDS.equals(item.getId().getSubscriptionUrl())) {
					result.add(item.toUserAggregatorSettings());
				} else {
					result.add(item.toFeedAggregatorSettings());
				}
			}
		} finally {
			s.close();
		}
		return result;
	}

	@Override
	public void deleteUserRelatedAggregatorSettings(String userId) {
		Session s = null;
		try {
			s = getSession();
			List<HAggregatorSettings> items = runListQuery("SELECT as FROM HAggregatorSettings as "+
				"WHERE as.id.userId = :userId", HAggregatorSettings.class, "userId", userId);
			s.close();
			s = getSession();
			for(HAggregatorSettings item : items) {
				s.delete(item);
			}
		} finally {
			s.close();
		}
	}

	@Override
	public void deleteFeedAggregatorSettings(String userId, String url) {
		Session s = null;
		try {
			s = getSession();
			s.delete(new HAggregatorSettings(userId, url));
		} finally {
			s.close();
		}
	}

}
