package com.sc.myrss.dao.hibernate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.sc.myrss.dao.exception.ItemNotFoundException;
import com.sc.myrss.dao.hibernate.provider.SessionProvider;

public class BaseHDAO {

	protected Session getSession() {
		return SessionProvider.getInstance().getSession();
	}

	protected <T> T runSingleSelectQuery(String query, Class<T> returnClass, Object... args) {
		T result = null;
		Session s = null;
		try {
			s = getSession();
			if(s==null) throw new RuntimeException("No session found");
			TypedQuery<T> q = s.createQuery(query, returnClass);
			injectArgsIntoQuery(q, args);
			result = q.getSingleResult();
		} catch(NoResultException e) {
			throw new ItemNotFoundException();
		} catch(Throwable t) {
			throw t;
		} finally {
			if(s!=null) s.close();
		}
		return result;
	}

	protected <T> List<T> runListQuery(String query, Class<T> returnClass, Object... args) {
		List<T> result = new ArrayList<T>();
		Session s = null;
		try {
			s = getSession();
			if(s==null) throw new RuntimeException("No session found");
			TypedQuery<T> q = s.createQuery(query, returnClass);
			injectArgsIntoQuery(q, args);
			result.addAll(q.getResultList());
		} catch(Throwable t) {
			throw t;
		} finally {
			if(s!=null) s.close();
		}
		return result;
	}

	protected void runUpdateQuery(String query, Object... args) {
		Session s = null;
		Transaction tx = null;
		try {
			s = getSession();
			if(s==null) throw new RuntimeException("No session found");
			tx = s.beginTransaction();
			Query q = s.createQuery(query);
			injectArgsIntoQuery(q, args);
			q.executeUpdate();
			tx.commit();
		} catch(Throwable t) {
			if(tx!=null && tx.isActive()) {
				tx.rollback();
			}
			throw t;
		} finally {
			if(s!=null) s.close();
		}
	}

	private void injectArgsIntoQuery(Query q, Object... args) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		for (int c=0; c<args.length; c+=2) {
			String key = args[c].toString();
			if (c+1 < args.length) {
				Object value = args[c+1];
				parameters.put(key, value);
			}
		}
		for(String key : parameters.keySet()) {
			q.setParameter(key, parameters.get(key));
		}
	}

}
