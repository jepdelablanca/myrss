package com.sc.myrss.dao.hibernate.factory;

import org.springframework.stereotype.Component;

import com.sc.myrss.dao.AggregatorDAO;
import com.sc.myrss.dao.factory.AggregatorDAOFactory;
import com.sc.myrss.dao.hibernate.AggregatorHDAO;

@Component
public class AggregatorHDAOFactoryImpl implements AggregatorDAOFactory {

	@Override
	public AggregatorDAO getDAO() {
		return new AggregatorHDAO();
	}

}
