package com.sc.myrss.dao.hibernate.vo;

import java.io.Serializable;
import java.util.Objects;

public class HRoleSecResource implements Serializable {

	private static final long serialVersionUID = -4343856395059324025L;

	private String roleId;

	private String secResourceId;

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getSecResourceId() {
		return secResourceId;
	}

	public void setSecResourceId(String secResourceId) {
		this.secResourceId = secResourceId;
	}

	@Override
	public String toString() {
		return "RoleSecResource [roleId=" + roleId + ", secResourceId=" + secResourceId + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(roleId, secResourceId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof HRoleSecResource))
			return false;
		HRoleSecResource other = (HRoleSecResource) obj;
		return Objects.equals(roleId, other.roleId) && Objects.equals(secResourceId, other.secResourceId);
	}

}
