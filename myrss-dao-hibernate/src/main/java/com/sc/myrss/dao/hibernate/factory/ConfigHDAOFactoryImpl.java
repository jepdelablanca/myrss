package com.sc.myrss.dao.hibernate.factory;

import org.springframework.stereotype.Component;

import com.sc.myrss.dao.ConfigDAO;
import com.sc.myrss.dao.factory.ConfigDAOFactory;
import com.sc.myrss.dao.hibernate.ConfigHDAO;

@Component
public class ConfigHDAOFactoryImpl implements ConfigDAOFactory {

	@Override
	public ConfigDAO getDAO() {
		return new ConfigHDAO();
	}

}
