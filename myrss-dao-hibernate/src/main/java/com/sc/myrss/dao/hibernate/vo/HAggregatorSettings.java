package com.sc.myrss.dao.hibernate.vo;

import java.io.Serializable;

import com.sc.myrss.model.rss.aggregator.FeedAggregatorSettings;
import com.sc.myrss.model.rss.aggregator.ServerAggregatorSettings;
import com.sc.myrss.model.rss.aggregator.UserAggregatorSettings;

public class HAggregatorSettings implements Serializable {

	private static final long serialVersionUID = -7212242634500092543L;

	public static final String SYSTEM_USER = "SYSTEM";

	public static final String ALL_FEEDS = "ALL";

	private HAggregatorSettingsId id;

	private String fetchRateCronExpression;

	private Long readItemsGuardTime;

	public HAggregatorSettings() {}

	public HAggregatorSettings(String userId, String url) {
		this();
		this.id = new HAggregatorSettingsId(userId, url);
	}

	public HAggregatorSettings(ServerAggregatorSettings source) {
		this();
		this.id = new HAggregatorSettingsId(SYSTEM_USER, ALL_FEEDS);
		this.fetchRateCronExpression = source.getFetchRateCronExpression();
		this.readItemsGuardTime = source.getReadItemsGuardTime();
	}

	public HAggregatorSettings(UserAggregatorSettings source) {
		this();
		this.id = new HAggregatorSettingsId(source.getUserId(), ALL_FEEDS);
		this.fetchRateCronExpression = source.getFetchRateCronExpression();
		this.readItemsGuardTime = source.getReadItemsGuardTime();
	}

	public HAggregatorSettings(FeedAggregatorSettings source) {
		this();
		this.id = new HAggregatorSettingsId(source.getUserId(), source.getUrl());
		this.fetchRateCronExpression = source.getFetchRateCronExpression();
		this.readItemsGuardTime = source.getReadItemsGuardTime();
	}

	public ServerAggregatorSettings toServerAggregatorSettings() {
		return new ServerAggregatorSettings(fetchRateCronExpression, readItemsGuardTime);
	}

	public UserAggregatorSettings toUserAggregatorSettings() {
		return new UserAggregatorSettings(id.getUserId(), 
				fetchRateCronExpression, readItemsGuardTime);
	}

	public FeedAggregatorSettings toFeedAggregatorSettings() {
		return new FeedAggregatorSettings(id.getUserId(), id.getSubscriptionUrl(), 
				fetchRateCronExpression, readItemsGuardTime);
	}

	public HAggregatorSettingsId getId() {
		return id;
	}

	public void setId(HAggregatorSettingsId id) {
		this.id = id;
	}

	public String getFetchRateCronExpression() {
		return fetchRateCronExpression;
	}

	public void setFetchRateCronExpression(String fetchRateCronExpression) {
		this.fetchRateCronExpression = fetchRateCronExpression;
	}

	public Long getReadItemsGuardTime() {
		return readItemsGuardTime;
	}

	public void setReadItemsGuardTime(Long readItemsGuardTime) {
		this.readItemsGuardTime = readItemsGuardTime;
	}
}
